#include "main.h"

int main(int argc, char const *argv[]) {
  
  char palabra_secreta[LARGO_MAXIMO_PALABRA];
  char resultado[LARGO_MAXIMO_MENSAJE];

  if (argc != 2) {
    printf("Uso del ejecutable: %s <file>\n", argv[0]);
    return 1;
  }

  elegir_palabra(argv[1], palabra_secreta);
  ahorcado(palabra_secreta, resultado);
  printf("\nFin del ahorcado\n%s\n", resultado);
  printf("La palabra era: %s\n", palabra_secreta);

  return 0;
}