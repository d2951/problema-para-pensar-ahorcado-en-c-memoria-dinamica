#include "entrada.h"

FILE* abrir_archivo(const char nombre_archivo[], char modo[]) {
  FILE *archivo = fopen(nombre_archivo, modo);
  assert(archivo != NULL);
  
  return archivo;
}

void elegir_palabra(const char nombre_archivo[], char palabra_secreta[]) {

  char linea[LARGO_MAXIMO_LINEA];
  char palabras[CANT_MAX_PALABRAS][LARGO_MAXIMO_LINEA];

  FILE *archivo_objeto = abrir_archivo(nombre_archivo, "r");

  int cant_palabras = 0;
  int largo_linea = 0;
  for(int i = 0; EOF != fscanf(archivo_objeto, "%s\n", linea); i++) {
    largo_linea = strlen(linea);
    strncpy(palabras[i], linea, largo_linea);
    palabras[i][largo_linea] = '\0';
    cant_palabras++;
  }

  fclose(archivo_objeto);

  int indice_palabra_secreta = numero_aleatorio(0, cant_palabras);
  strncpy(palabra_secreta, palabras[indice_palabra_secreta],
    strlen(palabras[indice_palabra_secreta]) + 1);
}

int numero_aleatorio(int minimo, int maximo) {
  srand(time(NULL));

  int tamano_intervalo = minimo - maximo + 1;
  return (rand() % tamano_intervalo) + minimo;
}