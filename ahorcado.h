#ifndef AHORCADO_H
#define AHORCADO_H

#define LARGO_MAXIMO_MENSAJE 40
#define VIDAS_AHORCADO 7

#include "entrada.h"

#ifdef _WIN32
  #define limpiar "cls"
#else // Unix, Mac
  #define limpiar "clear"
#endif

/*
 * ahorcado contiene la lógica del juego ahorcado
 */
void ahorcado(char palabra_secreta[], char resultado[]);

/*
 * iniciar_tablero toma la palabra secreta y una cadena a modo de buffer
 * y guarda allí una secuencia de guiones del largo de la palabra secreta
 */
void iniciar_tablero(char palabra_secreta[], char estado_juego[]);

/*
 * mostrar_tablero toma el estado del juego y lo muestra en pantalla
 */
void mostrar_tablero(char palabra_secreta[]);

/*
 * mostrar_vidas toma la cantidad actual de vidas y la muestra en pantalla
 */
void mostrar_vidas(int vidas);

/*
 * pedir_letra toma una letra por entrada estándar
 */
char pedir_letra();

/*
 * juego_continua toma un caracter y devuelve 0 en caso de que la letra
 * argumento sea '1'. Caso contrario devuelve 1.
 */
int juego_continua(char letra);

/*
 * modificar_tablero toma una palabra secreta, el estado actual del
 * juego y una letra *l*. Modifica el estado del juego agregando todas las
 * letras *l*. Si el estado se modificó devuelve 0, caso contrario devuelve -1.
 */
int modificar_tablero(char palabra_secreta[], char estado_juego[], char letra);

/*
 * juego_ganado toma la palabra secreta y el estado del juego y devuelve
 * 1 en el caso que sean iguales, 0 en caso contrario.
 */
int juego_ganado(char palabra_secreta[], char estado_juego[]);

/*
 * generar_mensaje_ganador toma un buffer para mensaje y la cantidad de vidas
 * restantes al finalizar el juego. Genera el mensaje "Ganaste! Te sobraron x vidas"
 * donde x es la cantidad de vidas pasada argumento.
 */
void generar_mensaje_ganador(char resultado[], int vidas);

#endif
